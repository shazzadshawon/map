package biz.agvcorp.mapfirlmy;

import android.location.Location;

import com.firebase.geofire.GeoLocation;
import com.google.android.gms.maps.model.Marker;

/**
 * Created by Shazzad on 2/1/2018.
 */

public class NearbyUser {
    public String user_id;
    public GeoLocation location;
    public Marker marker;

    public NearbyUser(){

    }

    public NearbyUser(String id){
        user_id = id;
    }

    public void setLocation(GeoLocation loc){
        location = loc;
    }

    public void setID(String id){
        user_id = id;
    }

    public String getID(){
        return user_id;
    }

    public void setMarker(Marker m){
        marker = m;
    }

    public Marker getMarker(){
        return marker;
    }

    public GeoLocation getLocation(){
        return  location;
    }
}
