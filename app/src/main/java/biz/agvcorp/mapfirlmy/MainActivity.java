package biz.agvcorp.mapfirlmy;

import android.content.Intent;
import android.graphics.drawable.RippleDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.LinearLayout;

public class MainActivity extends AppCompatActivity {

    private Button mLogin;
    private Button mRegistration;
    private Button mPhoneNumberRegistration;

    private LinearLayout linearLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        linearLayout = findViewById(R.id.mainWindow);

        mLogin = findViewById(R.id.btn_login_lan);
        mRegistration = findViewById(R.id.btn_registration_lan);
        mPhoneNumberRegistration = findViewById(R.id.btn_phoneNumber_lan);

        final Intent loginIntent = new Intent(MainActivity.this, SignInActivity.class);
        final Intent regIntent = new Intent(MainActivity.this, SignUpActivity.class);
        final Intent phoneIntent = new Intent(MainActivity.this, SplashActivity.class);

        mLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            new Handler().post(new Runnable() {
                @Override
                public void run() {
                    startActivity(loginIntent);
                    finish();
                }
            });
            }
        });

        mRegistration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            new Handler().post(new Runnable() {
                @Override
                public void run() {
                    startActivity(regIntent);
                    finish();
                }
            });
            }
        });

        mPhoneNumberRegistration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            new Handler().post(new Runnable() {
                @Override
                public void run() {
                    startActivity(phoneIntent);
                    finish();
                }
            });
            }
        });
    }
}
