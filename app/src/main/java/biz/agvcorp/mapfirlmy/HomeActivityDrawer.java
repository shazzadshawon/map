package biz.agvcorp.mapfirlmy;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class HomeActivityDrawer extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private FirebaseAuth mAuth;
    private DrawerLayout drawer;

    SharedPreferences pref;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_drawer);

        mAuth = FirebaseAuth.getInstance();

        pref = getApplicationContext().getSharedPreferences("MyPref", 0);
        editor = pref.edit();

        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.drawer_contnet_main, new MapDrawerFragment()).commit();

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home_activity_drawer, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        FragmentManager fragmentManager = getFragmentManager();

        //noinspection SimplifiableIfStatement
        switch (id){
            case R.id.action_settings:
                fragmentManager.beginTransaction().replace(R.id.drawer_contnet_main, new ProfileEditDrawerFragment()).commit();
                break;
            case R.id.action_logout:
                logout();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.
        drawer.closeDrawer(GravityCompat.START);
        final int id = item.getItemId();
        final FragmentManager fragmentManager = getFragmentManager();
        final Fragment profileEditFragment = new ProfileEditDrawerFragment();
        final Fragment profileFragment = new ProfileDrawerFragment();
        final Fragment homeFragment = new MapDrawerFragment();

        new Handler().post(new Runnable() {
            @Override
            public void run() {
                switch (id){
                    case R.id.nav_map:
                        fragmentManager.beginTransaction().replace(R.id.drawer_contnet_main, homeFragment).commit();
                        break;
                    case R.id.nav_profile:
                        fragmentManager.beginTransaction().replace(R.id.drawer_contnet_main, profileFragment).commit();
                        break;
                    case R.id.nav_home:
                        fragmentManager.beginTransaction().replace(R.id.drawer_contnet_main, profileEditFragment).commit();
                        break;
                    case R.id.nav_logout:
                        logout();
                        break;
                }
            }
        });

        //DrawerLayout drawer = findViewById(R.id.drawer_layout);
        return true;
    }

    private void logout(){
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0);
        SharedPreferences.Editor editor = pref.edit();
        editor.clear();
        editor.commit();

        logoutFromFirebase();

        Intent logoutIntent = new Intent(HomeActivityDrawer.this, SignInActivity.class);
        startActivity(logoutIntent);
        finish();
    }

    private void logoutFromFirebase(){
        String user_id = mAuth.getCurrentUser().getUid();
        FirebaseAuth.getInstance().signOut();

        if (user_id != null){
            DatabaseReference current_user_db = FirebaseDatabase.getInstance().getReference().child("Users").child("Customers").child(user_id);
            current_user_db.removeValue();
        } else {
            DatabaseReference current_user_db = FirebaseDatabase.getInstance().getReference().child("Users").child("Customers").child(getCurrentId());
            current_user_db.removeValue();
        }
    }

    private String getCurrentId(){
        return pref.getString("logginID", "no");
    }
}
