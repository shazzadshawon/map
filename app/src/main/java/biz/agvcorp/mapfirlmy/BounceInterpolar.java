package biz.agvcorp.mapfirlmy;

/**
 * Created by Shazzad on 2/8/2018.
 */

public class BounceInterpolar implements android.view.animation.Interpolator {
    double mAmplitude = 1;
    double mFrequency = 10;

    public BounceInterpolar(double amplitude, double frequency) {
        mAmplitude = amplitude;
        mFrequency = frequency;
    }

    @Override
    public float getInterpolation(float time) {
        double amplitude = mAmplitude;
        if (amplitude == 0) { amplitude = 0.05; }

        // The interpolation curve equation:
        //    -e^(-time / amplitude) * cos(frequency * time) + 1
        //
        // View the graph live: https://www.desmos.com/calculator/6gbvrm5i0s
        return (float) (-1 * Math.pow(Math.E, -time/ mAmplitude) * Math.cos(mFrequency * time) + 1);
    }
}
