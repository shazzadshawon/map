package biz.agvcorp.mapfirlmy;

import android.animation.ObjectAnimator;
import android.animation.TypeEvaluator;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.VectorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.Property;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.firebase.geofire.GeoFire;
import com.firebase.geofire.GeoLocation;
import com.firebase.geofire.GeoQuery;
import com.firebase.geofire.GeoQueryEventListener;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.location.places.ui.SupportPlaceAutocompleteFragment;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.maps.android.SphericalUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Shazzad on 1/25/2018.
 */

public class MapDrawerFragment extends Fragment implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, com.google.android.gms.location.LocationListener, GoogleMap.OnCameraMoveStartedListener, GoogleMap.OnCameraMoveListener, GoogleMap.OnCameraMoveCanceledListener, GoogleMap.OnCameraIdleListener, GoogleMap.OnMyLocationButtonClickListener{
    SharedPreferences pref;
    SharedPreferences.Editor editor;

    View mView;
    View mBottomSheet;
    View mMapAutoCompleteBottomSheet;

    private BottomSheetBehavior mBehiviour;
    private BottomSheetBehavior mMapAutocompleteBehaiviour;

//    MapFragment mapFragment;
    MapView mapFragment;

    private GoogleMap mMap;
    FusedLocationProviderClient mFusedLocationClient;
    GoogleApiClient mGoogleApiClient;
    LocationRequest mLocationRequest;

    private Boolean isMapGestured = false;
    private Boolean isMarkerRotating = false;

    Location mLastLocation;
    private Task<Location> mInitCurrentLocation;
    Location mMovedLocation;
    double mMovedZoom, mMovedTilt, mMovedBearing;

    final int LOCATION_REQUEST_CODE = 1;

    Marker userMarker;

    GeoQuery geoQuery;
    private float radiusforallnearby;
    private List<NearbyUser> users = new ArrayList<NearbyUser>();
    private List<Marker> markerMap = new ArrayList<Marker>();
    private NearbyUser nearbyUser;
    private Marker allUserMarker;

    private LatLngInterpolator mLatLngInterpolator = new LatLngInterpolator.Spherical();
//    private LatLngInterpolator mLatLngInterpolator = new LatLngInterpolator.LinearFixed();

    private TextView mMap_bottomSheet_currAblDriver;

    private Button mMapLocStartBtn;
    private RelativeLayout mMapLocContainer;
    private EditText mLocationPickup, mLocationDestination;
    private int REQUEST_CODE_AUTOCOMPLETE = 1;

    private String destination, pickupPoint;
    private LatLng destinationLatLng, pickupPointLatLng;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.map_activity_drawer, container, false);

        pref = mView.getContext().getSharedPreferences("MyPref", 0);
        editor = pref.edit();

        LatLngInterpolator latLngInterpolator;

        return mView;
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(getActivity());
        radiusforallnearby = (float) 50.0;

//        mapFragment = (MapFragment) getChildFragmentManager().findFragmentById(R.id.gmaps);
        mapFragment = view.findViewById(R.id.gmaps);
        mapFragment.onCreate(savedInstanceState);
        mapFragment.onResume();

        if (ActivityCompat.checkSelfPermission(view.getContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(view.getContext(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, LOCATION_REQUEST_CODE);
        } else {
            mapFragment.getMapAsync(this);
        }

        mBottomSheet = view.findViewById(R.id.bottom_sheet);
        mMap_bottomSheet_currAblDriver = mBottomSheet.findViewById(R.id.map_bottomSheet_currAblDriver);
        mBehiviour = BottomSheetBehavior.from(mBottomSheet);
        mBehiviour.setHideable(false);
        mBehiviour.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                switch (newState) {
                    case BottomSheetBehavior.STATE_DRAGGING:
                        Log.i("BottomSheetCallback", "BottomSheetBehavior.STATE_DRAGGING");
                        break;
                    case BottomSheetBehavior.STATE_SETTLING:
                        Log.i("BottomSheetCallback", "BottomSheetBehavior.STATE_SETTLING");
                        break;
                    case BottomSheetBehavior.STATE_EXPANDED:
                        Log.i("BottomSheetCallback", "BottomSheetBehavior.STATE_EXPANDED");
                        break;
                    case BottomSheetBehavior.STATE_COLLAPSED:
                        Log.i("BottomSheetCallback", "BottomSheetBehavior.STATE_COLLAPSED");
                        break;
                    case BottomSheetBehavior.STATE_HIDDEN:
                        Log.i("BottomSheetCallback", "BottomSheetBehavior.STATE_HIDDEN");
                        break;
                }

            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                Log.i("BottomSheetCallback", "slideOffset: " + slideOffset);
            }
        });
//        mMapAutoCompleteBottomSheet = view.findViewById(R.id.map_autocomplete);
//        mMapAutocompleteBehaiviour = BottomSheetBehavior.from(mMapAutoCompleteBottomSheet);

        mMapLocContainer = view.findViewById(R.id.map_loc_container);
        mMapLocStartBtn = view.findViewById(R.id.map_starter_button);
        mMapLocStartBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mMapLocStartBtn.setVisibility(View.GONE);
                mMapLocContainer.setVisibility(View.VISIBLE);
            }
        });

        mLocationPickup = view.findViewById(R.id.map_loc_pickup);
        mLocationDestination = view.findViewById(R.id.map_loc_destination);
        TextWatcher pickupTW = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                requestSuggestion(charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        };
        mLocationPickup.addTextChangedListener(pickupTW);


        /*final PlaceAutocompleteFragment placeAutocompleteFragmentPickup = (PlaceAutocompleteFragment) getChildFragmentManager().findFragmentById(R.id.map_loc_pickup);
        final PlaceAutocompleteFragment placeAutocompleteFragmentDestination = (PlaceAutocompleteFragment) getChildFragmentManager().findFragmentById(R.id.map_loc_destination);
        placeAutocompleteFragmentPickup.getView().findViewById(R.id.place_autocomplete_search_input).setAlpha((float) 0.8);
        placeAutocompleteFragmentPickup.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                pickupPoint = place.getName().toString();
                pickupPointLatLng = place.getLatLng();
            }

            @Override
            public void onError(Status status) {

            }
        });
        placeAutocompleteFragmentDestination.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                destination = place.getName().toString();
                destinationLatLng = place.getLatLng();
            }

            @Override
            public void onError(Status status) {

            }
        });
        placeAutocompleteFragmentPickup.getView().findViewById(R.id.place_autocomplete_clear_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    Intent intent = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN).build(getActivity());
                    startActivityForResult(intent, REQUEST_CODE_AUTOCOMPLETE);
                } catch (GooglePlayServicesRepairableException e) {
                    e.printStackTrace();
                } catch (GooglePlayServicesNotAvailableException e) {
                    e.printStackTrace();
                }

                ((EditText) placeAutocompleteFragmentPickup.getView().findViewById(R.id.place_autocomplete_search_input)).setText("");
                ((EditText) placeAutocompleteFragmentPickup.getView().findViewById(R.id.place_autocomplete_search_input)).setHint("Please Set Your Pick Up Point");
            }
        });
        placeAutocompleteFragmentDestination.getView().findViewById(R.id.place_autocomplete_clear_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    Intent intent = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN).build(getActivity());
                    startActivityForResult(intent, REQUEST_CODE_AUTOCOMPLETE);
                } catch (GooglePlayServicesRepairableException e) {
                    e.printStackTrace();
                } catch (GooglePlayServicesNotAvailableException e) {
                    e.printStackTrace();
                }

                ((EditText) placeAutocompleteFragmentDestination.getView().findViewById(R.id.place_autocomplete_search_input)).setText("");
                ((EditText) placeAutocompleteFragmentDestination.getView().findViewById(R.id.place_autocomplete_search_input)).setHint("Please Set Your Destination Point");
            }
        });*/
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Find myLocationButton view
        @SuppressLint("ResourceType") View myLocationButton = mapFragment.findViewById(0x2);

        if (myLocationButton != null && myLocationButton.getLayoutParams() instanceof RelativeLayout.LayoutParams) {
            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) myLocationButton.getLayoutParams();
            // Align it to - parent BOTTOM|LEFT
            params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
            params.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
            params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, 0);
            params.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0);
            // Update margins, set to 10dp
            final int margin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 80, getResources().getDisplayMetrics());
            params.setMargins(margin, margin, margin, margin);
            myLocationButton.setLayoutParams(params);
        }

        /*ATTACHING GOOGLE MAP CAMERA LISTENER*/
        mMap.setOnCameraIdleListener(this);
        mMap.setOnCameraMoveStartedListener(this);
        mMap.setOnCameraMoveListener(this);
        mMap.setOnCameraMoveCanceledListener(this);

        if (ActivityCompat.checkSelfPermission(mView.getContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mView.getContext(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, LOCATION_REQUEST_CODE);
        }

        /*GOOGLE MAP STYLEING SILVER*/
        /*googleMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(mView.getContext(), R.raw.gmap_silver_style));*/
        /*GOOGLE MAP STYLEING RETRO*/
        googleMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(mView.getContext(), R.raw.gmap_retro_style));
        /*GOOGLE MAP STYLEING UBER*/
        /*googleMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(mView.getContext(), R.raw.gmap_uber_style));*/

        /*Enabling Location Provider Services of GOOGLE*/

        buildGoogleApiClient();
        mMap.setMyLocationEnabled(true);

    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(mView.getContext())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    LocationCallback mLocationCallback = new LocationCallback() {
        @Override
        public void onLocationResult(LocationResult locationResult) {
            for (Location location : locationResult.getLocations()) {
                updateMap(location);
            }
        }
    };

    public void updateMap(Location location){
        mLastLocation = location;
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());

        if (mMovedLocation != null){
            LatLng movedLatlng = new LatLng(mMovedLocation.getLatitude(), mMovedLocation.getLongitude());

            CameraPosition movedCameraPosition = new CameraPosition.Builder()
                    .target(movedLatlng)
                    .zoom((float) mMovedZoom)
                    .bearing((float) mMovedBearing)
                    .tilt((float) mMovedTilt)
                    .build();

//            Log.d("mMovedLocation", mMovedLocation.toString());
            mMap.animateCamera(CameraUpdateFactory.newCameraPosition(movedCameraPosition));
//            mMap.moveCamera(CameraUpdateFactory.newLatLng(movedLatlng));
//            mMap.animateCamera(CameraUpdateFactory.zoomTo((float) mMovedZoom));
        } else {
            CameraPosition currentCameraPosition = new CameraPosition.Builder()
                    .target(latLng)
                    .zoom(16)
                    .bearing(location.getBearing())
                    .tilt(0)
                    .build();

//            Log.d("originalLocation", latLng.toString());
            mMap.animateCamera(CameraUpdateFactory.newCameraPosition(currentCameraPosition));
//            mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
//            mMap.animateCamera(CameraUpdateFactory.zoomTo(16));
        }

        removeMarker(userMarker);
        userMarker = addMarker(mLastLocation, getCurrentId()+", You Are Here!");
        userMarker.showInfoWindow();
        updateFirebaseUserLocation(location);
        updatePickupLocationBar(location);
    }

    public void updateFirebaseUserLocation(Location location){
        String userId = FirebaseAuth.getInstance().getCurrentUser().getUid();
        if (!getCurrentId().equals("no")){
            if (!userId.equals(getCurrentId())){
                userId = getCurrentId();
            }
        }
        DatabaseReference refCustomer = FirebaseDatabase.getInstance().getReference().child("Users").child("Customers");
        DatabaseReference refCustomerBearing = FirebaseDatabase.getInstance().getReference().child("Users").child("Customers").child(userId);
        GeoFire geoFireCustomer = new GeoFire(refCustomer);
        geoFireCustomer.setLocation(userId, new GeoLocation(location.getLatitude(), location.getLongitude()));
        HashMap map = new HashMap();
        map.put("bearing", location.getBearing());
        refCustomerBearing.updateChildren(map);
    }

    private void updatePickupLocationBar(Location loc){
        Geocoder gc = new Geocoder(mView.getContext());
        try {
            List<Address> locName = gc.getFromLocation(loc.getLatitude(), loc.getLongitude(), 4);
            Log.d("DefaultLocation", locName.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        /*GETTING THE LAST KNOW LOCATION OF THE USER FROM GOOGLE*/
        if (ActivityCompat.checkSelfPermission(mView.getContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mView.getContext(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, LOCATION_REQUEST_CODE);
        }
        mFusedLocationClient.getLastLocation().addOnSuccessListener(getActivity(), new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                if (location != null){
                    //Log.d("InitLastLocation", location.toString());
                    mMovedLocation = location;
                    mMovedZoom = 16;
                    mMovedBearing = location.getBearing();
                    mMovedTilt = 0.0;

                    getAlluserLocation(location);
                }
            }
        });

        requestLocationUpdates();
    }

    public void requestLocationUpdates() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(100);
        mLocationRequest.setFastestInterval(100);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
//        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);

        if (ActivityCompat.checkSelfPermission(mView.getContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mView.getContext(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, LOCATION_REQUEST_CODE);
        }
        mFusedLocationClient.requestLocationUpdates(mLocationRequest, mLocationCallback, Looper.myLooper());
    }

    private void getAlluserLocation(Location location){
        DatabaseReference allUserRef = FirebaseDatabase.getInstance().getReference().child("Users").child("Customers");
        GeoFire geoFireCustomer = new GeoFire(allUserRef);
        GeoFire geoFire = new GeoFire(allUserRef);
        geoQuery = geoFire.queryAtLocation(new GeoLocation(location.getLatitude(), location.getLongitude()), radiusforallnearby);
        geoQuery.removeAllListeners();

        geoQuery.addGeoQueryEventListener(new GeoQueryEventListener() {
            @Override
            public void onKeyEntered(String key, GeoLocation location) {
                if (!key.equals(getCurrentId())){
                    Location loc = new Location(LocationManager.GPS_PROVIDER);
                    loc.setLatitude(location.latitude);
                    loc.setLongitude(location.longitude);
                    Marker marker = addCarMarker(loc, "This is "+key);
                    dropMarker(marker);
                    marker.setFlat(false);
                    marker.showInfoWindow();

                    nearbyUser = new NearbyUser();
                    nearbyUser.setID(key);
                    nearbyUser.setLocation(location);
                    nearbyUser.setMarker(marker);
                    users.add(nearbyUser);
                    mMap_bottomSheet_currAblDriver.setText(String.valueOf(users.size() / 2));

                    if ( users.size() > 0 ){

                        if ( !users.get(users.size()-1).user_id.equals(key) ){

                        }
                    } else {
                        /*Location loc = new Location(LocationManager.GPS_PROVIDER);
                        loc.setLatitude(location.latitude);
                        loc.setLongitude(location.longitude);
                        Marker marker = addCarMarker(loc, "This is "+key);
                        //dropMarker(marker);
                        marker.setFlat(false);
                        marker.showInfoWindow();

                        nearbyUser = new NearbyUser();
                        nearbyUser.setID(key);
                        nearbyUser.setLocation(location);
                        nearbyUser.setMarker(marker);
                        users.add(nearbyUser);*/
                    }
                }
            }

            @Override
            public void onKeyExited(String key) {
                if (!key.equals(getCurrentId())){
                    if ( users.size() > 0 ){
                        for (int i = 0; i < users.size(); i++){
                            if ( users.get(i).getID().equals(key) ){
                                removeMarker( users.get(i).getMarker() );
                                users.remove(i);
                            }
                        }
                    } else {
                        /*I dont care right now*/
                    }
                }
            }

            List<LatLng> points = new ArrayList<LatLng>();
            private int pointsCounter = 0;
            @Override
            public void onKeyMoved(final String key, final GeoLocation location) {
                if (!key.equals(getCurrentId())){

                    /*Google Roadr Web services api key AIzaSyC8Z3kCzwnpe_rfgDAs1lYydGuT9WY6HZ0*/
                    /*Google Roads API is not free. 2500 free request per day and 50 request per second.*/
                    if (pointsCounter < 5){
                        pointsCounter++;
                        points.add(new LatLng(location.latitude, location.longitude));
                        return;
                    } else {
                        pointsCounter = 0;

                        if ( users.size() > 0 ){
                            for (int i = 0; i < users.size(); i++){
                                if (users.get(i).getID().equals(key)){
                                    //removeMarker(users.get(i).getMarker());
                                /*if (users.get(i).getMarker() != null){
                                    users.get(i).getMarker().remove();
                                }*/

                                    Location loc = findMidpointLoc(points);
                                    LatLng from = users.get(i).getMarker().getPosition();
                                    points.clear();

                                    Location prev_loc = new Location(LocationManager.GPS_PROVIDER);
                                    prev_loc.setLatitude(from.latitude);
                                    prev_loc.setLongitude(from.longitude);

//                                    Float heading = (float) SphericalUtil.computeHeading(from, to);
                                    float bearing = prev_loc.bearingTo(loc) + 90;   /*94*/
                                    rotateAnimateMarker(users.get(i).getMarker(), bearing, new LatLng(loc.getLatitude(), loc.getLongitude()));
//                                    ObjectAnimator markerRotation = ObjectAnimator.ofFloat(users.get(i).getMarker() , "rotation", 0f, 360f);
//                                    markerRotation.setDuration(1000); // miliseconds
//                                    markerRotation.start();
//                                    users.get(i).getMarker().setAnchor(0.5f, 0.5f);
//                                    users.get(i).getMarker().setRotation(bearing);

//                                    animateMarkerToICS(users.get(i).getMarker(), new LatLng(loc.getLatitude(), loc.getLongitude()), mLatLngInterpolator);

                                    /*Marker marker = addCarMarker(loc, "This is "+key);
                                    marker.showInfoWindow();
                                    users.get(i).setMarker(marker);*/
                                }
                            }
                        } else {

                        }

                    }
                }
            }

            @Override
            public void onGeoQueryReady() {
//                Log.d("Initiated", "All initial data has been loaded and events have been fired!");
            }

            @Override
            public void onGeoQueryError(DatabaseError error) {

            }
        });
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onCameraIdle() {

    }

    @Override
    public void onCameraMoveCanceled() {

    }

    @Override
    public void onCameraMove() {
        if (!isMapGestured){
            return;
        }
        CameraPosition newLocation = mMap.getCameraPosition();
        mMovedLocation.setLatitude(newLocation.target.latitude);
        mMovedLocation.setLongitude(newLocation.target.longitude);
        mMovedZoom = newLocation.zoom;
        mMovedTilt = newLocation.tilt;
        mMovedBearing = newLocation.bearing;
//        Log.d("OriginalCamarePosition", mLastLocation.toString() );
    }

    @Override
    public void onCameraMoveStarted(int reason) {
        if (reason == GoogleMap.OnCameraMoveStartedListener.REASON_GESTURE) {
//            Toast.makeText(mView.getContext(), "The user gestured on the map.", Toast.LENGTH_SHORT).show();
            isMapGestured = true;
        } else if (reason == GoogleMap.OnCameraMoveStartedListener.REASON_API_ANIMATION) {

        } else if (reason == GoogleMap.OnCameraMoveStartedListener.REASON_DEVELOPER_ANIMATION) {

        }
    }

    @Override
    public void onResume() {
        super.onResume();
        super.onResume();
        if (mGoogleApiClient != null && mFusedLocationClient != null) {
            requestLocationUpdates();
        } else {
            buildGoogleApiClient();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mFusedLocationClient != null) {
            mFusedLocationClient.removeLocationUpdates(mLocationCallback);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode){
            case LOCATION_REQUEST_CODE:{
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    mapFragment.getMapAsync(this);
                } else {
                    Toast.makeText(mView.getContext(), "Please Give the Permission to Use the App any Further", Toast.LENGTH_LONG).show();
                }
                break;
            }
        }
    }

    @Override
    public boolean onMyLocationButtonClick() {
        isMapGestured = true;
        return false;
    }

    private Marker addMarker(Location location, String title){
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        Bitmap bitmap = getBitmap(mView.getContext(), R.drawable.ic_mapuser);
        return mMap.addMarker(new MarkerOptions().position(latLng).title(title). icon(BitmapDescriptorFactory.fromBitmap(bitmap)).draggable(true) );
    }

    private Marker addCarMarker(Location location, final String title){
        final LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        /*Bitmap bitmap = getBitmap(mView.getContext(), R.drawable.ic_cartop);*/
        Bitmap bitmap = getBitmap(mView.getContext(), R.drawable.ic_cartop4);
        return mMap.addMarker(new MarkerOptions().position(latLng).title(title). icon(BitmapDescriptorFactory.fromBitmap(bitmap)).draggable(true) );
    }

    private void removeMarker(Marker marker){
        if (marker != null){
            marker.remove();
        }
    }

    /*FOR SGV AS MARKER .................................................*/
    private static Bitmap getBitmap(Context context, int drawableId) {
        Drawable drawable = ContextCompat.getDrawable(context, drawableId);
        if (drawable instanceof BitmapDrawable) {
            return BitmapFactory.decodeResource(context.getResources(), drawableId);
        } else if (drawable instanceof VectorDrawable) {
            return getBitmap((VectorDrawable) drawable);
        } else {
            throw new IllegalArgumentException("unsupported drawable type");
        }
    }
    private static Bitmap getBitmap(VectorDrawable vectorDrawable) {
        Bitmap bitmap = Bitmap.createBitmap(vectorDrawable.getIntrinsicWidth(),
                vectorDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        vectorDrawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        vectorDrawable.draw(canvas);
        return bitmap;
    }
    /*FOR SGV AS MARKER .................................................*/

    private String getCurrentId(){
        return pref.getString("logginID", "no");
    }
    private String getCurrentToken(){
        return pref.getString("logginToken", "no");
    }
    private String getCurrentFirebaseUserID(){
        return pref.getString("firebaseUserID", "no");
    }

    private Location findMidpointLoc(List<LatLng> points){
        double latidutes = 0;
        double longitudes = 0;
        int n = points.size();

        for (LatLng point : points){
            latidutes += point.latitude;
            longitudes += point.longitude;
        }

        Location loc = new Location(LocationManager.GPS_PROVIDER);
        loc.setLatitude(latidutes/n);
        loc.setLongitude(longitudes/n);
        return loc;
    }

    static void animateMarkerToICS(Marker marker, LatLng finalPosition, final LatLngInterpolator latLngInterpolator) {
        TypeEvaluator<LatLng> typeEvaluator = new TypeEvaluator<LatLng>() {
            @Override
            public LatLng evaluate(float fraction, LatLng startValue, LatLng endValue) {
                return latLngInterpolator.interpolate(fraction, startValue, endValue);
            }
        };
        Property<Marker, LatLng> property = Property.of(Marker.class, LatLng.class, "position");
        ObjectAnimator animator = ObjectAnimator.ofObject(marker, property, typeEvaluator, finalPosition);

        animator.setDuration(4000);
        animator.start();
    }

    private void rotateAnimateMarker(final Marker marker, final float toRotation, final LatLng newLoc) {
        if(!isMarkerRotating) {
            final Handler handler = new Handler();
            final long start = SystemClock.uptimeMillis();
            final float startRotation = marker.getRotation();
            final long duration = 1500;

            final Interpolator interpolator = new LinearInterpolator();

            handler.post(new Runnable() {
                @Override
                public void run() {
                    isMarkerRotating = true;

                    long elapsed = SystemClock.uptimeMillis() - start;
                    float t = interpolator.getInterpolation((float) elapsed / duration);

                    float rot = t * toRotation + (1 - t) * startRotation;
                    marker.setAnchor(0.5f, 0.5f);
                    marker.setRotation(-rot > 180 ? rot / 2 : rot);
                    if (t < 1.0) {
                        handler.postDelayed(this, 20);
                    } else {
                        isMarkerRotating = false;
                        animateMarkerToICS(marker, newLoc, mLatLngInterpolator);
                    }
                }
            });
        }
    }

    void dropMarker(final Marker marker) {
        final LatLng finalPosition = new LatLng(marker.getPosition().latitude, marker.getPosition().longitude);

        Projection projection = mMap.getProjection();
        Point startPoint = projection.toScreenLocation(finalPosition);
        startPoint.y = 0;
        final LatLng startLatLng = projection.fromScreenLocation(startPoint);
        final Interpolator interpolator = new BounceInterpolar(0.11, 4.6);

        TypeEvaluator<LatLng> typeEvaluator = new TypeEvaluator<LatLng>() {
            @Override
            public LatLng evaluate(float fraction, LatLng startValue, LatLng endValue) {
                float t = interpolator.getInterpolation(fraction);
                double lng = t * finalPosition.longitude + (1 - t) * startLatLng.longitude;
                double lat = t * finalPosition.latitude + (1 - t) * startLatLng.latitude;
                return new LatLng(lat, lng);
            }
        };

        Property<Marker, LatLng> property = Property.of(Marker.class, LatLng.class, "position");
        ObjectAnimator animator = ObjectAnimator.ofObject(marker, property, typeEvaluator, finalPosition);
        animator.setDuration(1000);
        animator.start();
    }

    private void requestSuggestion(String input){
        final String url = "https://maps.googleapis.com/maps/api/place/autocomplete/json?input="+input+"&types=geocode&language=bn&key="+getString(R.string.GooglePlaceAPIKey);

        RequestQueue placeSuggestion = Volley.newRequestQueue(mView.getContext());

        JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    int id;
                    String name;
                    JSONArray array = response.getJSONArray("predictions");
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject row = array.getJSONObject(i);
                        Log.d("SuggestionResponse", row.toString());
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Snackbar.make(mView, "Google Response: Google Server Down", Snackbar.LENGTH_SHORT).show();
            }
        });

        placeSuggestion.add(postRequest);
    }
}


/*Android Bottom Sheets tutorial https://stackoverflow.com/questions/38291397/a-sliding-up-panel-just-like-google-maps-app*/
